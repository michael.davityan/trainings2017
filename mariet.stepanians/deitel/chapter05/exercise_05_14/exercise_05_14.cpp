#include <iostream>

int
main()
{
    double total = 0.0;
    
    while (true) {
        int productNumber;
        std::cout << "Enter the product number from 1 to 5 (-1 to quit): ";
        std::cin >> productNumber;
        if (-1 == productNumber) {
            break;
        } 
        if (productNumber >= 6 || productNumber <= 0) {
            std::cout << "Error 1: There is no such product number!!" << std::endl;
            return 1;
        }
        
        int productQuantity;
        std::cout << "Enter the quantity of sold products: ";
        std::cin >> productQuantity;
        if (productQuantity < 0) {
            std::cerr << "Error 2: The quantity of sold products cannot be negative!!" << std::endl;
            return 2;
        }

        switch (productNumber) {
        case 1: total += productQuantity * 2.98; break;
        case 2: total += productQuantity * 4.50; break;
        case 3: total += productQuantity * 9.98; break;
        case 4: total += productQuantity * 4.49; break;
        case 5: total += productQuantity * 6.87; break;
        }
    }

    std::cout << "Total --> $" << total << std::endl;
    return 0;
}
