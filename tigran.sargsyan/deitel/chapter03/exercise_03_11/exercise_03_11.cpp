#include "GradeBook.hpp"

#include <iostream>
#include <string>

int
main()
{
    GradeBook gradeBook1("CS101 Introduction to C++ programming", "Artak Yenokyan");
    gradeBook1.displayMessage();

    return 0;
}

