#include <iostream>

int 
main()
{
    while (true) {
        double workedHours;
        std::cout << "Enter Hours worked (-1 to end): ";
        std::cin >> workedHours;

        if (-1 == workedHours) {
            break;
        }

        double hourlyRate;
        std::cout << "Enter hourly rate of the worker ($00.00): ";
        std::cin >> hourlyRate;
        
        double salary = workedHours * hourlyRate;
        if (workedHours > 40) {
            /// salary = 1.5 * salary - 20 * hourlyRate;
            salary += 0.5 * hourlyRate * (workedHours - 40);
        }
        std::cout << "Salary: " << salary << "\n";
    }

    return 0;
}
