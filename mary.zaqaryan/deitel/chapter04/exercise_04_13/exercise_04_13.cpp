#include <iostream>

int 
main()
{
    double totalDistance = 0;
    double totalGasoline = 0;
    
    while (true) {
        double miles;
        std::cout << "Enter the miles used (-1 to exit): ";
        std::cin >> miles;
        if (-1 == miles) {
            break;
        }

        double gallons;
        std::cout << "Enter gallons: ";
        std::cin >> gallons;

        std::cout << "MPG this tankful: " << miles / gallons << std::endl;

        totalDistance += miles;
        totalGasoline += gallons;
        
        std::cout << "Total miles/gallon: " << totalDistance / totalGasoline << "\n\n";
    }

    return 0;
}
