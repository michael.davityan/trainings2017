#include<iostream>

int
main()
{
    double radius;

    std::cout << "Enter the radius of a circle: ";
    std::cin >> radius;

    if (radius <= 0) {
        std::cerr << "Error 1: Radius can't be negative or zero." << std::endl;
        return 1;
    }

    double pi = 3.14159;
    double circumference = 2 * pi * radius;
    double area = pi * radius * radius;
    double diameter = 2 * radius;

    std::cout << "The diameter of a circle is " << diameter << std::endl;
    std::cout << "The circumference of a circle is " << circumference << std::endl;
    std::cout << "The area of a circle is " << area << std::endl;

    return 0;
}
