#include <iostream>

int
main()
{
    { ///a)
        int number;
                            
        std::cout << "Enter a positive integer: ";
        std::cin >> number;

        int counter = 1;
        int factorial = 1;

        while (counter <= number) {
            factorial *= counter;
            ++counter;
        }   

        std::cout << "Factorial of " << number << " is " << factorial << std::endl;
    }
    
    { ///b)
        int accuracy;
    
        std::cout << "Enter a accuracy of e: ";
        std::cin >> accuracy;

        double eFactorial = 1;
        double factorial = 1;
        int counter = 1;

        while (counter <= accuracy) {
            factorial *= counter;
            eFactorial += 1 / factorial;
            ++counter;
        }

        std::cout << "e is " << eFactorial << std::endl;
    }

    { ///c)
        int degree;

        std::cout << "Enter a degree of e: ";
        std::cin >> degree;

        int accuracy;
    
        std::cout << "Enter a accuracy of e^" << degree << ": ";
        std::cin >> accuracy;

        double eFactorial = 1;
        double factorial = 1;
        int counter = 1;
        int multiple = 1;

        while (counter <= accuracy) {
            factorial *= counter;
            multiple *= degree;
            eFactorial += multiple / factorial;
            ++counter;
        }
        
        std::cout << "e is " << eFactorial << std::endl;
    }

    return 0;
}

