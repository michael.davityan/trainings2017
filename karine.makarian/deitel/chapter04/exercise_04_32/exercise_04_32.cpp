#include <iostream>

int
main()
{
    double sideA;
            
    std::cout << "Enter the first side of a triangle: ";
    std::cin >> sideA;

    if (sideA <= 0) {
        std::cerr << "Error 1: Triangle side can't be zero or negative." << std::endl;
        return 1;
    }
                        
    double sideB;
                            
    std::cout << "Enter the second side of a triangle: ";
    std::cin >> sideB;
                                    
    if (sideB <= 0) {
        std::cerr << "Error 1: Triangle side can't be zero or negative." << std::endl;
        return 1;
    }

    double sideC;

    std::cout << "Enter the third side of a triangle: ";
    std::cin >> sideC;

    if (sideC <= 0) {
        std::cerr << "Error 1: Triangle side can't be zero or negative." << std::endl;
        return 1;
    }

    if(sideA < sideB + sideC) {
        if(sideB < sideA + sideC) {
            if(sideC < sideB + sideA) {
                std::cout << "Triangle can be formed." << std::endl;
                return 0;
            } 
        } 
    } 

    std::cout << "Triangle can't be formed." << std::endl;
    return 0;
}

