class Account
{
public:    
    Account(int initialBalance);
    void setBalance(int initialBalance);
    int credit(int amount);
    int debit(int amount);
    int getBalance();
    
private:
    int balance_;
};

