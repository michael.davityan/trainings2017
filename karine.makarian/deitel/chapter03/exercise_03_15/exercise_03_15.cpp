#include "Date.hpp"
#include <iostream>

int
main()
{
    Date date(2, 23, 1998);

    std::cout << "date's year is " << date.getYear() << std::endl;
    std::cout << "date's month is " << date.getMonth() << std::endl; 

    date.setMonth(15);

    std::cout << "date's month after setting a number 15 is " << date.getMonth() << std::endl;
    std::cout << "the date is "; 
    date.displayDate();
    return 0;
}
