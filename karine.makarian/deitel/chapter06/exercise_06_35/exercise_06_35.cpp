#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

void
printAnswer(bool answerStatus) 
{
    if (answerStatus) {
        std::cout << "Very good!\n"; 
    } else {
        std::cout << "No. Please try again.: ";
    }   
}

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(std::time(0));
    }

    bool answerStatus = true;
    int first = 0;
    int second = 0;
    while (true) {
        if (answerStatus) {
            first = 1 + std::rand() % 10;
            second = 1 + std::rand() % 10;
        }

        if (::isatty(STDIN_FILENO)) {
            std::cout << "How much is " << first << " times " << second << " ? ";
        }

        int answer;
        std::cin >> answer;
        if (-1 == answer) {
            break;
        }
        
        answerStatus = (answer == first * second);
        printAnswer(answerStatus);
    
    }

    return 0;
}

