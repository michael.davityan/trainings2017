#include <iostream>
#include <iomanip>

int
celsius(const int fahrenheit) 
{
    return (fahrenheit - 32) * 5 / 9;
}

int
fahrenheit(const int celsius) 
{
    return celsius * 9 / 5 + 32;
}

int
main()
{
    for (int count = 1; count <= 100; ++count) {
        std::cout << "Celsius: " << count << " Fahrenheit: "<< fahrenheit(count) << std::endl;
    }
            
    std::cout << std::endl;

    for (int count = 32; count <= 212; ++count) {
        std::cout << "Fahrenheit: " << count << " Celsius: "<< celsius(count) << std::endl;
    }

    return 0;
}

