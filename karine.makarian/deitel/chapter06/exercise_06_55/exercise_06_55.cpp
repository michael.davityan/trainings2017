#include <iostream>
#include <cassert>

double 
circleArea(const double radius) 
{
    assert(radius >= 0);
    return 3.14159 * radius * radius;
}

int
main()
{
    double radius;
    std::cin >> radius;
    
    if (radius < 0) {
        std::cerr << "Error 1: Non positive value was entered." << std::endl;
        return 1; 
    }

    std::cout << "Area of the circle is " << circleArea(radius) << std::endl; 
    
    return 0;
}
