#include <iostream>

int main()
{
    int c; ///must be char c; because cin.get() takes only characters, not integer values.
        
    if ( ( с = cin.get() ) != EOF )
    {
        main();
        std::cout << c;
    }
    
    return 0;
}

Line cout << c   in all main() recursive steps this line work until we press ctrl+d(EOF).
But they stay in background because of first main() recursive call. 
After pressing ctrl+d all recursive called main() cout's are  shown on display.


