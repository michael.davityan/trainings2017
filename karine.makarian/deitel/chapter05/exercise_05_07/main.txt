#include <iostream>

int 
main(
{
    int x; 
    int y; 
    
    std::cout << "Enter two integers in the range 1-20: ";
    std::cin >> x >> y; 
        
    for ( int i = 1; i <= y; i++ ) 
    {
        for ( int j = 1; j <= x; j++ ) 
            std::cout << '@';
                
            std::cout << std::endl;
    } 
        
    return 0; 
}

Output is a matrix, where x produces the  count of columns and y produces the count of rows.
If x = 3; y = 5; the output will be
@@@
@@@
@@@
@@@
@@@
