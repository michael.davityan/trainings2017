#include <iostream>

int
main()
{
    double total = 0;
    while (true) {
        int productNumber;
        int quantitySold;
    
        std::cout << "Enter a pair of number: ";
        std::cin >> productNumber >> quantitySold;
        std::cout << std::endl;

        if (-1 == productNumber || -1 == quantitySold) {
            break;
        }

        if (quantitySold < 0) {
            std::cerr << "Error 1: Quantity can't be non positive." << std::endl;
            return 1;
        }
    
        switch (productNumber) {
        case 1: total += quantitySold * 2.98; break;
        case 2: total += quantitySold * 4.50; break;
        case 3: total += quantitySold * 9.98; break;
        case 4: total += quantitySold * 4.49; break;
        case 5: total += quantitySold * 6.87; break;
        default:
            std::cerr << "Error 2: Invalid product number." << std::endl;
            return 2;
        }
    }

    std::cout << "Total is " << total << std::endl;

    return 0;
}

