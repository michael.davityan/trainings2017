#include <iostream>
#include <iomanip>

int
main()
{
    for (int percent = 5; percent <= 10; ++percent) {
        double rate = percent / 100.0;
        double amountRate = 1.0 + rate;
        double amount = 24.0;
        std::cout << std::fixed << "Rate: " << percent << '%' << std::endl
            << "-------------------------------------" << std::endl
            << "Year      |" << std::setw(25) << "Amount " << std::endl
            << "-------------------------------------" << std::endl;

        for (int year = 1626; year <= 2018; ++year) {
            amount *= amountRate;
            std::cout << year << std::setw(7) << '|' << std::setw(25) << amount << std::endl;
        }
    
        std::cout << "-------------------------------------" << std::endl;
        std::cout << std::endl;
  }

    return 0;
}

