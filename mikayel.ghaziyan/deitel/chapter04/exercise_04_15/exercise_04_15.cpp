#include <iostream>
#include <iomanip>

int
main()
{
    while (true) {
        double sales = 0;
        std::cout << "Enter sales dollars (-1 to end): ";
        std::cin >> sales >> std::setprecision(2) >> std::fixed;
        if (-1 == sales){
            return 0;
        }
        if (sales < -1) {
            std::cerr << "Error 1: the amount of sales cannot be a negative number." << std::endl;
            return 1;
        }

        double salaryWeekly = 200.00;
        double salaryTotal = salaryWeekly + (sales * 0.09);
        std::cout << "Salary is: $" << salaryTotal << std::setprecision(2) << std::fixed << std::endl;
    } 
    return 0;
}
		  
