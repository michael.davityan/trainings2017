#include <iostream>

int
main()
{
    for (int i = 1; i <= 5; i++) {
        int number;
        std::cout << "Enter number: ";
        std::cin >> number;
        if (number < 1) {
            std::cout << "Error 1: Number cannot be smaller than 1." << std::endl;
            return 1;
        }
        if (number > 30) {
            std::cout << "Error 2: Number cannot be bigger than 30." << std::endl;
            return 2;
        }
        
        for (int j = 1; j <= number; j++) {
            std::cout << "*";
        } 
        std::cout << std::endl;
    }
    return 0;
}
