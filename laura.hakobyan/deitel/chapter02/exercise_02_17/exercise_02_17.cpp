 ///Print the numbers on the same line with one space
 #include <iostream> /// allows program to perform input and output
 int 
 main()
 {
    std::cout << "a) Using one statement with one stream insertion operator: " << "1 2 3 4 \n";
    std::cout << "b) Using one statement with four stream insertion operators: " << "1 " << "2 " << "3 " << " 4 \n";
    std::cout << "c) Using four statements: " << "1 ";
    std::cout <<  "2 "; 
    std::cout <<  "3 "; 
    std::cout <<  "4 " << std::endl; 
    
    return 0;       /// indicate program executed successfully
 }

