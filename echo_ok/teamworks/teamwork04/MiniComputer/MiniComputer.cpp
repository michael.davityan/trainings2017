#include "MiniComputer.hpp"
#include <iostream>
#include <string>

MiniComputer::MiniComputer(std::string name)
{
    setComputerName(name);
}

std::string
MiniComputer::getComputerName()
{
    return computerName_;
}

void
MiniComputer::setComputerName(std::string name)
{
    if (name.length() > 25) {
        computerName_ = name.substr(0, 25);
        return;
    }

    computerName_ = name;
}

int
MiniComputer::run()
{
    std::cout << getComputerName() << std::endl; /// getter

start:
    showMainMenu();
    int command = getMainCommand();
    if (0 == command) { /// exit command
        return 0;
    }
    int returnValue = executeCommand(command);
    if (0 == returnValue) { /// everything is ok
        goto start;
    }
    return returnValue;
}

void
MiniComputer::showMainMenu()
{
    std::cout << std::endl;
    std::cout << "Command Set\n"
              << "0 – exit\n"
              << "1 – load\n"
              << "2 – store\n"
              << "3 – add\n"
              << "4 – print\n\n"
              << "> Command: ";
}

int
MiniComputer::getMainCommand()
{
    int command;
    std::cin >> command;
    return command;
}

int
MiniComputer::executeCommand(int command)
{
    if (1 == command) { /// load command
        return executeLoadCommand();
    }
    if (2 == command) { /// store command
        return executeStoreCommand();
    }
    if (3 == command) { /// add command
        return executeAddCommand();
    }
    if (4 == command) { /// Print command
        return executePrintCommand();
    }
    /// error case
    std::cerr << "Error 1: Command not found!" << std::endl;
    return 1;
}

int
MiniComputer::loadIntoRegister(std::string variableName)
{    
    int variableValue;
    std::cout << "Load into register " << variableName << std::endl
              << "Input the value to load into register " << variableName << std::endl
              << "> " << variableName << ": ";
    std::cin  >> variableValue;
    std::cout << variableName << " = " << variableValue << std::endl;

    return variableValue;
}

int
MiniComputer::executeLoadCommand()
{
    int registerNumber;

    std::cout << "Load (into one of the registers)" << std::endl
              << "0 - a" << std::endl
              << "1 - b" << std::endl
              << "2 - c" << std::endl
              << "3 - d" << std::endl;
    std::cout << "> Register: ";
    std::cin  >> registerNumber;
    
    if (0 == registerNumber) {
        RegisterA_ = loadIntoRegister("a");
        return 0;
    }
    if (1 == registerNumber) {
        RegisterB_ = loadIntoRegister("b");
        return 0;
    }
    if (2 == registerNumber) {
        RegisterC_ = loadIntoRegister("c");
        return 0;
    }
    if (3 == registerNumber) {
        RegisterD_ = loadIntoRegister("d");
        return 0;
    }

    std::cerr << "Error 2: Register not found" << std::endl;
    return 2;
}

int
MiniComputer::executeStoreCommand()
{
    std::cout << "Error 127: The store command is not implemented yet" << std::endl;;
    return 127;
}

int
MiniComputer::getRegisterValue(int registerNumber) 
{    
    if (0 == registerNumber) {
        return RegisterA_;
    }
    if (1 == registerNumber) {
        return RegisterB_;
    }
    if (2 == registerNumber) {
        return RegisterC_;
    }
    if (3 == registerNumber) {
        return RegisterD_;
    }
    
    std::cerr << "Warning 1: Register not found" << std::endl;
    return 0;
}
    
void
MiniComputer::displayRegisterName(int registerNumber) 
{    
    if (0 == registerNumber) {
        std::cout << "a";
        return;
    }
    if (1 == registerNumber) {
        std::cout << "b";
        return;
    }
    if (2 == registerNumber) {
        std::cout << "c";
        return;
    }
    if (3 == registerNumber) {
        std::cout << "d";
        return;
    }
    std::cout << "Warning 1: Register not found" << std::endl;
}

int
MiniComputer::executeAddCommand()
{
    int registerNumber1;

    std::cout << "Add (Register 1 + Register 2 = Register 3)"   << std::endl
              << "0 - a" << std::endl
              << "1 - b" << std::endl
              << "2 - c" << std::endl
              << "3 - d" << std::endl
              << "> Register 1: ";
    std::cin  >> registerNumber1;
    if (registerNumber1 > 3) {
        std::cout << "Error 2: Register not found" << std::endl;
        return 2;
    }    
    if (registerNumber1 < 0) {
        std::cout << "Error 2: Register not found" << std::endl;
        return 2;
    }
    
    int registerNumber2;

    std::cout << "> Register 2: ";
    std::cin  >> registerNumber2;
    if (registerNumber2 > 3) {
        std::cout << "Error 2: Register not found" << std::endl;
        return 2;
    }
    if (registerNumber2 < 0) {
        std::cout << "Error 2: Register not found" << std::endl;
        return 2;
    }
    
    int registerNumber3;

    std::cout << "> Register 3: ";
    std::cin  >> registerNumber3;
     
    int registerValue1 = getRegisterValue(registerNumber1);
    int registerValue2 = getRegisterValue(registerNumber2);
    int registerValue3 = getRegisterValue(registerNumber3);
                                
    if (0 == registerNumber3) {
        RegisterA_ = registerValue3 = registerValue1 + registerValue2;
    } 
    if (1 == registerNumber3) {
        RegisterB_ = registerValue3 = registerValue1 + registerValue2;
    } 
    if (2 == registerNumber3) {
        RegisterC_ = registerValue3 = registerValue1 + registerValue2;
    }
    if (3 == registerNumber3) {
        RegisterD_ = registerValue3 = registerValue1 + registerValue2;
    } 

    
    displayRegisterName(registerNumber3);
    std::cout << " = ";
    displayRegisterName(registerNumber1);
    std::cout << " + ";
    displayRegisterName(registerNumber2);
    std::cout << " = ";
    std::cout << registerValue1 << " + " << registerValue2 << " = " << registerValue3;
    
    return 0;
}

int
MiniComputer::executePrintCommand()
{
    int printValue;    
   
    std::cout << "Print (Register or String)" << std::endl
              << "0 - Register" << std::endl
              << "N - String Length" << std::endl
              << "\n> Print: ";
    std::cin  >> printValue;
    
    if (0 == printValue) { 
        int registerNumber;
        std::cout << "Registers" << std::endl
                  << "0 - a" << std::endl
                  << "1 - b" << std::endl
                  << "2 - c" << std::endl
                  << "3 - d" << std::endl
                  << std::endl
                  << "Register: ";
        std::cin  >> registerNumber;
        int loadToRegisterValue = getRegisterValue(registerNumber);
        std::cout << loadToRegisterValue << std::endl;

        return 0;
    }
    
    std::string printString;
    
    std::cout << "> String: ";
    std::cin.ignore(256, '\n');
    std::getline(std::cin, printString);

    if (printString.length() > printValue) {
        std::cout << printString.substr(0, printValue);
        return 0;
    }

    std::cout << printString;
           
    return 0;
}

