#include <iostream>

int
main()
{
    while (true) {
        int time;
        
        std::cout << "Please enter your work time (-1 to exit): ";
        std::cin >> time;

        if (-1 == time) {
            return 0;
        }
        if (time < 0) {
            std::cerr << "Error 1: Work time can't be negative: " << std::endl;
            return 1;
        }

        float money;

        std::cout << "Please enter your hourly rate ($00.00): $";
        std::cin >> money;

        if (money < 0) {
            std::cerr << "Error 2: Hourly rate can't be negative: " << std::endl;
            return 2; 
        }

        float salary = money * time;

        if (time > 40) {
            salary += (money / 2) * (time - 40);
        }

        std::cout << "Salary: $" << salary << std::endl;
    }

    return 0;
}
