#include <iostream>

int
main()
{
    while (true) {
        int accountNumber;

        std::cout << "Please enter your account number(-1 to exit): ";
        std::cin >> accountNumber;

        if (-1 == accountNumber) {
            return 0;
        }

        float initialBalance;
        
        std::cout << "Enter the initial balance: ";
        std::cin >> initialBalance;

        if (initialBalance < 0) {
            std::cerr << "Error 1: The initial balance can't be negative" << std::endl;
            return 1;
        }
        
        float outgoBalance;
        
        std::cout << "Enter the outgo balance: ";
        std::cin >> outgoBalance;
        
        if (outgoBalance < 0) {
            std::cerr << "Error 2: The outgo balance can't be negative" << std::endl;
            return 2;
        }

        float incomeBalance;
        
        std::cout << "Enter the income balance: ";
        std::cin >> incomeBalance;
        
        if (incomeBalance < 0) {
            std::cerr << "Error 3: The income balance can't be negative" << std::endl;
            return 3;
        }

        float limitCredit;
        
        std::cout << "Enter limit of credit: ";
        std::cin >> limitCredit;
        
        if (limitCredit < 0) {
            std::cerr << "Error 4: The limit of credit can't be negative" << std::endl;
            return 4;
        }

        float newBalance = initialBalance - outgoBalance + incomeBalance;

        std::cout << "New balance: " << newBalance << std::endl;

        if (newBalance > limitCredit) {
            std::cout << "Account: " << accountNumber << std::endl;
            std::cout << "Credit limit: " << limitCredit << std::endl;
            std::cout << "Balance: " << newBalance << std::endl;
            std::cout << "Credit Limit reached" << std::endl;
        }
    }
    
    return 0;
}
