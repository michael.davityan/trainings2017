#include <iostream>
#include <cstdlib>

int
main()
{
    int number = std::rand() % 1000 + 1;
    int guess;
    int counter = 1;

    std::cout << "Can you guess my number (1-1000)?" << std::endl
              << "My number is: ";
    std::cin >> guess;

    while (guess != number) {
        if (guess > number) {
            std::cout << "No, my number is lower than " << guess << std::endl
                      << "My number is: ";
            std::cin >> guess;
        } else {
            std::cout << "No, my number is higher than " << guess << std::endl
                      << "My number is: ";
            std::cin >> guess;
        }
        ++counter;
    }
    
    std::cout << "Correct! Congratulations you guessed my number" << std::endl;

    if (counter < 10) {
        std::cout << "You are lucky or you know the secret" << std::endl;
    } else if (10 == counter) {
        std::cout << "You know the secret" << std::endl;
    } else {
        std::cout << "You must train yourself" << std::endl;
    }

    return 0;
}
