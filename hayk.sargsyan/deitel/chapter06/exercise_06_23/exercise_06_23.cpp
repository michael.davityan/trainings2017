#include <iostream>
#include <assert.h>

int
drawSquare(const int side, const char fillCharacter) 
{
    assert(side > 0);
    for (int counterY = 1; counterY <= side; ++counterY) {
        for (int counterX = 1; counterX <= side; ++counterX) {
            std::cout << fillCharacter;
        }

        std::cout << std::endl;
    }

    return 0;
}

int
main()
{
    int side;

    std::cout << "Please enter the side: ";
    std::cin >> side;

    if (side < 1) {
        std::cerr << "Error 1: Side can't be zero on negative" << std::endl;
        return 1;
    }

    char fillCharacter;

    std::cout << "Please enter the fill character: ";
    std::cin >> fillCharacter;

    drawSquare(side, fillCharacter);

    return 0;
}
