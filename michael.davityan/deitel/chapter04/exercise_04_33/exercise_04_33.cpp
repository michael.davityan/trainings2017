#include <iostream>

int
main()
{
    /// This is the sides of triangle.
    int side1;
    int side2;
    int side3;

    std::cin >> side1 >> side2 >> side3;

    if (side1 < 0) {
        std::cout << "Error 1: wrong side." << std::endl;
        return 1;
    } 
    if (side2 < 0) {
        std::cout << "Error 1: wrong side." << std::endl;
        return 1;
    } 
    if (side3 < 0) {
        std::cout << "Error 1: wrong side." << std::endl;
        return 1;
    } 

    if ((side1 * side1) + (side2 * side2) == (side3 * side3)) {
        std::cout << "This is the sides of right triangle." << std::endl;
        return 0;
    } 
    if ((side3 * side3) + (side2 * side2) == (side1 * side1)) {
        std::cout << "This is the sides of right triangle." << std::endl;
        return 0;
    } 
    if ((side1 * side1) + (side3 * side3) == (side2 * side2)) {
        std::cout << "This is the sides of right triangle." << std::endl;
        return 0;
    } 

    std::cout << "This is not the sides of right triangle." << std::endl;
    return 0;
}
