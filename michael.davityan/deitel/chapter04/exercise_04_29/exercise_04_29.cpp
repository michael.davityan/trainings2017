#include <iostream>

int
main() 
{
    int base = 2;

    while (true) {
        std::cout << base << ", ";
        base *= 2;
    }

    std::cout << std::endl;
    return 0;
}
