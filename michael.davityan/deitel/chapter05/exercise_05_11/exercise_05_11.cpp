#include <iostream>
#include <iomanip>
#include <cmath> 

int 
main()
{
    std::cout << std::fixed << std::setprecision(2);
    for (int integerRate = 5; integerRate <= 10; ++integerRate) {
        double rate = integerRate * 0.01;
        double amount = 1000.0;
        std::cout << "==================================================================================\n"
                  << "------------------------This table for " 
                  << std::setw(2) << integerRate << "%" 
                  << " per annum -----------------------------\n"
                  << "==================================================================================\n";


        std::cout << "Year" << std::setw(21) << "Amount on deposit" << std::endl;
        for (int year = 1; year <= 10; ++year) {
            amount *= (1.0 + rate);
            std::cout << std::setw(4) << year << std::setw(21) << amount << std::endl;
        }
    }
    std::cout << "==================================================================================\n"
              << "==================================================================================" << std::endl;
    return 0;
}
