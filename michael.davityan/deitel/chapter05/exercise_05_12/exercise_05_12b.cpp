#include <iostream>
#include <iomanip>
int
main()
{
    std::cout << " (a)\t\t" << "(b)\t\t" << "(c)\t\t" << "(d)\t\t\n"; 
    int count = 10;
    for (int row = 1; row <= 10; ++row) {
        for (int column = 1; column <= row; ++column) {
            std::cout << "*";
        } 
        for (int column = count; column >= 1; --column) {
            std::cout << " ";
        }

        std::cout << "    ";

        for (int column = count; column >= 1; --column) {
            std::cout << "*";
        }
        for (int column = 1; column <= row; ++column) {
            std::cout << " ";
        } 

        std::cout << "   ";
          
        for (int column = 1; column <= row; ++column) {
            std::cout << " ";
        } 
        for (int column = count; column >= 1; --column) {
            std::cout << "*";
        }

        std::cout << "   ";

        for (int column = count; column >= 1; --column) {
            std::cout << " ";
        }
        for (int column = 1; column <= row; ++column) {
            std::cout << "*";
        }

        std::cout << std::endl; 
        --count;
    }

    return 0;
}

