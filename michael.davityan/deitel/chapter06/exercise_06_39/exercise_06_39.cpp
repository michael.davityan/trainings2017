#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

void printMessage(int answer, int number);
void printLowMessage();
void printHighMessage();
void printGuessMessage();
void printNewGameMessage();
void printMessageForGuessingLessThanTen();
void printMessageForGuessingMoreThanTen();
void printMessageForGuessingByTen();

int 
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(std::time(0));
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "I have a number between 1 and 1000.\n" 
                  << "Can you guess my number?\n"
                  << "Please type your first guess. ";
    }
    int number = 0;
    int attemptsQuantity = 0;
    bool answerStatus = true;
    while (true) {
        if (answerStatus) {
            number = 1 + std::rand() % 1000;
        }
        int answer;
        std::cin >> answer;
        answerStatus = (number == answer);
        printMessage(answer, number);
        if (answerStatus) {
            const int ATTEMPTS_LIMIT = 10;
            if (attemptsQuantity < ATTEMPTS_LIMIT) {
                printMessageForGuessingLessThanTen();
            }
            if (attemptsQuantity > ATTEMPTS_LIMIT) {
                printMessageForGuessingMoreThanTen();
            }
            if (attemptsQuantity == ATTEMPTS_LIMIT) {
                printMessageForGuessingByTen();
            }
            printGuessMessage();
            char answerForContinuation;
            std::cin >> answerForContinuation;
            if ((answerForContinuation != 'n') && (answerForContinuation != 'N') &&
                (answerForContinuation != 'y') && (answerForContinuation != 'Y')) {
                std::cerr << "Error 1: wrong answer. Must be y or n(lowercase or uppercase).\n";
                return 1; 
            }
            if (('n' == answerForContinuation) || ('N' == answerForContinuation)) {
                break;
            }
            if (('y' == answerForContinuation) || ('Y' == answerForContinuation)) {
                if(::isatty(STDIN_FILENO)) {
                    printNewGameMessage();
                }
            }
        } 
        ++attemptsQuantity;
    }
    return 0;
}

void 
printMessage(int answer, int number)
{
    if (answer < number) {
        printLowMessage();
    } 
    if (answer > number) {
        printHighMessage();
    }
}

void 
printLowMessage()
{
    std::cout << "Too low. Try again: ";
}

void 
printHighMessage()
{
    std::cout << "Too high. Try again: ";
}

void 
printGuessMessage()
{
    std::cout << "Would you like to play again (y or n)?: ";
}

void 
printMessageForGuessingLessThanTen()
{
    std::cout << "Either you know the secret or you got lucky!\n";
}

void 
printMessageForGuessingMoreThanTen()
{
    std::cout << "You should be able to do better!\n";
}

void 
printMessageForGuessingByTen()
{
    std::cout << "A hah! You know the secret!\n";
}

void 
printNewGameMessage()
{
    std::cout << "Can you guess my new number? ";
}
