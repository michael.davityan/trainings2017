Question!!!.
7.14 Find the error(s) in each of the following statements:
------------------------------------------------------------------------------------------------------
a. Assume that: char str[ 5 ];
                cin >> str; // user types "hello"
------------------------------------------------------------------------------------------------------
b. Assume that: int a[ 3 ];
                cout << a[ 1 ] << " " << a[ 2 ] << " " << a[ 3 ] << endl;
------------------------------------------------------------------------------------------------------
c. double f[ 3 ] = { 1.1, 10.01, 100.001, 1000.0001 };
------------------------------------------------------------------------------------------------------
d. Assume that: double d[ 2 ][ 10 ]; 
                d[ 1, 9 ] = 2.345;
======================================================================================================

Answer!!!.
------------------------------------------------------------------------------------------------------
a. I think much better if char str[6], for \0 string ending symbol!!!.
------------------------------------------------------------------------------------------------------
b. Wrong!!!. It must be: cout << a[0] << " " << a[1] << " " << a[2] << endl; 
------------------------------------------------------------------------------------------------------
c. Wrong!!!. It must be: double f[3] = {1.1, 10.01, 100.001}; 
This massive can hold only three elements.
------------------------------------------------------------------------------------------------------
d. Wrong!!!. The second line must be: d[1][9] = 2.345;
======================================================================================================
